[![pipeline status](https://gitlab.com/sofreeus/sofreeus.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/sofreeus/sofreeus.gitlab.io/commits/master)

# Software Freedom School primary web site

[site](https://www.sofree.us)

Please [open issues](https://gitlab.com/sofreeus/sofreeus.gitlab.io/issues) as you find problems, and feel free to contribute.
