# SFS Workstation VMs

SFS will publish an OVA of VirtualBox VMs, as a component to reliable and consistent student environments.

The archive will be refreshed monthly and uploaded to our NextCloud.

We will use the leading edges of the distributions LF supports in their certified sysadmin exam.

Currently:
- Ubuntu
- Fedora
- openSUSE

Each VM has the following stats:
- 4GB RAM
- 16GB HDD
- NAT (default)

The following are verified on each machine before release:
- osadmin has a standard password and is a global sudoer
- up-to-date with patches
- rebooted since last patch
- screen resizes automatically with window
- copy and paste works bi-directionally with the host
- sshd is not enabled
- machines are otherwise default installs

The following scripts are installed in /home/admin-scripts:
- cab (creates users Alice and Bob as global, password-less sudoers)
- stouith (Shoot The Other Users In The Head, deletes Alice, Bob, and osadmin, except for whoever's running the script)
- git-a-class (does whatever's needed to install a class to ~/git/some-class)
